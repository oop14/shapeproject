/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject;

/**
 *
 * @author Aritsu
 */
public class Triangle {
    private double h;
    private double b;
    public Triangle(double h, double b){
        this.h = h;
        this.b = b;
        
    }
    public double calArea(){
        return 0.5*h*b;
    }
    public double getH(){
        return h;
    }
    public double getB(){
        return b;
    }
    public void set(double h,double b){
        if (h>0 && b>0){
            this.h = h;
            this.b = b;
        }else{
            System.out.println("Error : h and b must more than zero !!");
            return;
        }
    }
}
