/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject;

/**
 *
 * @author Aritsu
 */
public class TestTriangle {
    public static void main(String[] args) {
        Triangle triangle1 = new Triangle(3,4);
        System.out.println("Area of triangle1 (h = "+triangle1.getH()+ ") , (b = "+triangle1.getB()+")  is " + triangle1.calArea());
        triangle1.set(4, 5);
        System.out.println("Area of triangle1 (h = "+triangle1.getH()+ ") , (b = "+triangle1.getB()+")  is " + triangle1.calArea());
        triangle1.set(0, 5);
        System.out.println("Area of triangle1 (h = "+triangle1.getH()+ ") , (b = "+triangle1.getB()+")  is " + triangle1.calArea());
        
    }
}
