/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject;

/**
 *
 * @author Aritsu
 */
public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(3,4);
        System.out.println("Area of rectangle1 (w = "+rectangle1.getW()+ ") , (h = "+rectangle1.getH()+")  is " + rectangle1.calArea());
        rectangle1.set(2, 5);
        System.out.println("Area of rectangle1 (w = "+rectangle1.getW()+ ") , (h = "+rectangle1.getH()+")  is " + rectangle1.calArea());
        rectangle1.set(4,0);
        System.out.println("Area of rectangle1 (w = "+rectangle1.getW()+ ") , (h = "+rectangle1.getH()+")  is " + rectangle1.calArea());
        
    }
}
